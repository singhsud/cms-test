package hello;

import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

@Component
//@Service
public class TestRevisionListener //implements RevisionListener
{

/*
    @Autowired
    public TestRevisionListener(RabbitmqQueuingService rabbitmqQueuingService){
        this.rabbitmqQueuingService = rabbitmqQueuingService;
    }
*/

    @PostPersist
    @PostUpdate
    @PostRemove
    private void afterAnyOperation(Object object) throws Exception {
        Test test = (Test) object;
        System.out.println(object);

        /* Since CustomRevisionListener is instantiated by a constructor in Envers,
        one have to find another way to retrieve a handle to a Spring managed bean.
        */

        RabbitmqQueuingService rabbitmqQueuingService = (RabbitmqQueuingService) ContextLookup.getBean("rabbitmqQueuingService");
        rabbitmqQueuingService.publishToTestQueue(test);
    }

    /*@Override
    public void newRevision(Object revisionEntity) {
        System.out.println("new revision " + revisionEntity);

    }*/
}
