package hello;


import org.springframework.data.repository.PagingAndSortingRepository;

public interface TestDao extends PagingAndSortingRepository<Test, Integer> {

    Test findByItemId(Integer id);

}
