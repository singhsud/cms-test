package hello;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.RabbitHealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class QueueConfiguration {

    @Value("${rabbit.address}")
    private String rabbitAddress;

    @Value("${rabbit.user}")
    private String rabbitUser;

    @Value("${rabbit.password}")
    private String rabbitPassword;


    @Value("${rabbit.testQ.consumers}")
    private String rabbitTestQConsumer;

/*    @Value("${rabbit.testQ.max_consumers}")
    private String rabbitTestQMaxConsumer;

    @Value("${rabbit.testQ.pre_fetch_count}")
    private String rabbitTestQPreFetchCount;
*/

    @Bean
    @Primary
    public ConnectionFactory connectionFactory() {
        com.rabbitmq.client.ConnectionFactory rabbitmqConnectionFactory = new com.rabbitmq.client.ConnectionFactory();
        rabbitmqConnectionFactory.setHost(rabbitAddress);
        rabbitmqConnectionFactory.setUsername(rabbitUser);
        rabbitmqConnectionFactory.setPassword(rabbitPassword);
        return new CachingConnectionFactory(rabbitmqConnectionFactory);
    }

   /* @Bean
    public SimpleRabbitListenerContainerFactory rabbitTestQueueListenerContainerFactory() {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrentConsumers(Integer.valueOf(rabbitTestQConsumer));
        factory.setMaxConcurrentConsumers(Integer.valueOf(rabbitTestQMaxConsumer));
        factory.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return factory;
    }*/


    @Bean
    public AmqpTemplate rabbitTemplate() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        RetryTemplate retryTemplate = new RetryTemplate();
        ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        retryTemplate.setBackOffPolicy(backOffPolicy);
        template.setRetryTemplate(retryTemplate);
        template.setChannelTransacted(true);
        return template;
    }

    @Bean
    public HealthIndicator addRMQHealthCheck() {
        return new RabbitHealthIndicator((RabbitTemplate) rabbitTemplate());
    }
}
