package hello;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Value("${test.rabbitmq.exchange}")
    private String testFanoutExchange;

    @Value("${test.rabbitmq.queue}")
    private String submittedTestQueue;

    @Bean
    Queue submittedTestQueue() {
        return new Queue(submittedTestQueue);
    }

    @Bean
    FanoutExchange testFanoutExchange() {
        return new FanoutExchange(testFanoutExchange);
    }

    @Bean
    Binding bindingTestQ(Queue submittedTestQueue, FanoutExchange testFanoutExchange) {
        return BindingBuilder.bind(submittedTestQueue).to(testFanoutExchange);
    }
}
