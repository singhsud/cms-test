package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/test/")
public class HelloController {

    @Value("${instance.name}")
    private String instanceName;

    @Autowired
    private TestService testService;

    @RequestMapping(value = "/")
    public String index() {
        return "Greetings from Spring Boot from instance "+instanceName;
    }

    @GetMapping(value = "/id/{id}")
    @ResponseBody
    public Test getById( @PathVariable("id") Integer id) {
        return testService.getById(id);
    }

    @PostMapping(value = "")
    @ResponseBody
    public Test create(@RequestBody Test test){
        return testService.create(test);
    }

}
