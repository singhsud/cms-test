package hello;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RabbitmqQueuingService {

    @Value("${test.rabbitmq.exchange}")
    private String testFanoutExchange;

    private RabbitTemplate rabbitTemplate;

    @Autowired
    public RabbitmqQueuingService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    public void publishToTestQueue(Test event) {
        log.info("publishing event: " + event);

        try {
            rabbitTemplate.convertAndSend(testFanoutExchange, "", new ObjectMapper().writeValueAsString(event));
        } catch (Exception e) {
            log.error("Exception occured:", e);
        }
    }
}


