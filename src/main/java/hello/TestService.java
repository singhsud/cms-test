package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Service("testService")
public class TestService implements Serializable {

    @Autowired
    private TestDao testDao;


    public Test getById(Integer id) {
        return testDao.findByItemId(id);
    }

    @Transactional
    public Test create(Test test) {
        return testDao.save(test);
    }
}
