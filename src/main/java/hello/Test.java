package hello;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test")
@Audited
@Data
@EntityListeners(TestRevisionListener.class)
//@RevisionEntity(TestRevisionListener.class)
public class Test //extends DefaultRevisionEntity
        implements Serializable {

    @Id
    Integer itemId;

    Integer restId;
    private static final long serialVersionUID = 1L;


/*    @GeneratedValue  // as DefaultRevisionEntity already do that
    @RevisionNumber
    private int id;

    @RevisionTimestamp
    private long timestamp;*/

}
