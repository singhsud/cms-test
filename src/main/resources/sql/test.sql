CREATE TABLE `test` (
  `item_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  PRIMARY KEY (`item_id`,`rest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `test_aud` (
  `item_id` int(11) NOT NULL DEFAULT '0',
  `rest_id` int(11) DEFAULT NULL,
  `rev` int(11) NOT NULL,
  `revtype` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`item_id`,`rev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
